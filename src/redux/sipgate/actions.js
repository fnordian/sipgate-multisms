import {postJson, requestSmsExtension} from './apihelper';

export const SEND_SMS = 'SEND_SMS';

export function sendSms(token, text, recipients) {

    console.log("token: " + token);

    return (dispatch) => {

        requestSmsExtension(token, (error, smsExtension) => {

            if (!error) {

                recipients.forEach((recipient) => {

                    dispatch({
                        type: SEND_SMS,
                        text
                    });


                    postJson({
                        recipient: recipient,
                        message: text,
                        smsId: smsExtension
                    }, 'https://api.sipgate.com/v1/sessions/sms', token, (error) => {
                        dispatch(receiveSendSmsResponse(error || "response!"));
                    });
                });
            }
        });
    }
}

export const CHANGE_RECIPIENT_NUMBER = 'CHANGE_RECIPIENT_NUMBER';

export function changeRecipientNumber(recipientIdx, recipientNumber) {
    console.log("number changing " + recipientIdx + " " + recipientNumber);
    return {
        type: CHANGE_RECIPIENT_NUMBER,
        recipientIdx, recipientNumber
    };
}

export const ADD_RECIPIENT_NUMBER = 'ADD_RECIPIENT_NUMBER';

export function addRecipientNumber() {
    return {
        type: ADD_RECIPIENT_NUMBER,
    };
}


export const REMOVE_RECIPIENT_NUMBER = 'REMOVE_RECIPIENT_NUMBER';

export function removeRecipientNumber(recipientIdx) {
    return {
        type: REMOVE_RECIPIENT_NUMBER,
        recipientIdx
    };
}

export const CHANGE_SMS_MESSAGE = 'CHANGE_SMS_MESSAGE';

export function changeMessage(message) {
    return {
        type: CHANGE_SMS_MESSAGE,
        message
    };
}

export const RECEIVE_SEND_SMS_RESPONSE = 'RECEIVE_SEND_SMS_RESPONSE';

function receiveSendSmsResponse(response) {
    return {
        type: RECEIVE_SEND_SMS_RESPONSE,
        response,
        receivedAt: Date.now()
    }
}


