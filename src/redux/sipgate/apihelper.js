export const postJson = (data, url, token, cb) => {
    const http = new XMLHttpRequest();
    http.open("POST", url, true);

    http.setRequestHeader("Content-type", "text/json");
    http.setRequestHeader("Authorization", "Bearer " + token);

    http.onreadystatechange = function () {//Call a function when the state changes.
        if (http.readyState === 4 && http.status === 200) {
            cb();
        } else {
            cb({error: true});
        }
    };
    http.send(JSON.stringify(data));

};


export const getJson = (url, token, cb) => {
    const http = new XMLHttpRequest();
    http.open("GET", url, true);

    console.log(`yo yo: ${url}`);

    http.setRequestHeader("Authorization", "Bearer " + token);

    http.onreadystatechange = function () {//Call a function when the state changes.
        if (http.readyState === 4) {
            if (http.status === 200) {
                const data = JSON.parse(http.responseText);
                cb(null, data);
            } else {
                cb({error: true});
            }
        }
    };
    http.send();

};


const getWebuserExtension = (token, cb) => {
    getJson("https://api.sipgate.com/v1/authorization/userinfo", token, (error, data) => {
        console.log(error);
        console.log(data);
        const webuserExtension = error ? null : data.sub;
        console.log(`webuser: ${webuserExtension} error: ${error}`);
        cb(error, webuserExtension);
    })
};


const getSmsExtensionForWebuser = (token, webuserExtension, cb) => {
    console.log(`got ${webuserExtension}`);
    const url = `https://api.sipgate.com/v1/${webuserExtension}/sms`;
    console.log(`url: ${url}`);
    getJson(url, token, (error, data) => {
        const arrayGetOr = (a, idx, or) => (a.length > idx ? a[idx] : or);
        const smsExtension = error ? null : arrayGetOr(data.items.filter((i) => i.id.indexOf("s") !== -1),0, {id:null}).id;
        cb(error, smsExtension);
    })
};



export const requestSmsExtension = (token, cb) => {
    getWebuserExtension(token, (error, webuserExtension) => {
        getSmsExtensionForWebuser(token, webuserExtension, (error, smsExtension) => {
            if (error) {
                cb(error, null);
            } else {
                cb(smsExtension ? null : {error: true}, smsExtension);
            }
        });
    })

};

