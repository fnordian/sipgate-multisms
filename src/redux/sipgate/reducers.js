import {RECEIVE_SEND_SMS_RESPONSE, SEND_SMS, CHANGE_RECIPIENT_NUMBER, ADD_RECIPIENT_NUMBER, REMOVE_RECIPIENT_NUMBER, CHANGE_SMS_MESSAGE} from "./actions";


function smsSender(state = {sending: false, pending: 0, recipients: [""], message: ""}, action) {
    console.log("action: " + action.type);
    switch (action.type) {
        case SEND_SMS:
            return {...state, sending: true, pending: state.pending + 1};
        case RECEIVE_SEND_SMS_RESPONSE:
            const pending = state.pending - 1;
            return {...state, sending: pending > 1, pending: state.pending - 1};
        case CHANGE_RECIPIENT_NUMBER:
            const newRecipients = Array.from(state.recipients);
            newRecipients[action.recipientIdx] = action.recipientNumber;
            return {...state, recipients: newRecipients};
        case ADD_RECIPIENT_NUMBER:
            const moreRecipients = Array.from(state.recipients);
            moreRecipients.push("");
            console.log("add");
            return {...state, recipients: moreRecipients};
        case REMOVE_RECIPIENT_NUMBER:
            const lessRecipients = Array.from(state.recipients);
            if (lessRecipients.length > 1) {
                lessRecipients.splice(action.recipientIdx, 1);
            }
            return {...state, recipients: lessRecipients};
        case CHANGE_SMS_MESSAGE:
            return {...state, message: action.message};

        default:
            return state;
    }
}


export default smsSender;
