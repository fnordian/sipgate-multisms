import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { authMiddleware, authReducer as auth } from 'redux-implicit-oauth2'
import sipgate from './sipgate/reducers'

const configureStore = (initialState) =>
    createStore(
        combineReducers({
            // other reducers
            sipgate,
            auth
        }),
        initialState,
        applyMiddleware(
            // other middleware
            thunk,
            authMiddleware
        )
    )

export default configureStore
