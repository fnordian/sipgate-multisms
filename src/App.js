import React, {Component} from "react";
import { Provider } from 'react-redux';
import "./App.css";
import storeFactory from './redux';
import Login from './components/Login';
import SmsSendStatus from './components/SmsSendStatus';
import SmsSendButton from './components/SmsSendButton';
import SmsSendForm from "./components/SmsSendForm";

const store = storeFactory({});

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="App">
                    <Login />
                    <br />
                    <SmsSendStatus />
                    <SmsSendButton />
                    <SmsSendForm />
                </div>
            </Provider>
        );
    }
}

export default App;
