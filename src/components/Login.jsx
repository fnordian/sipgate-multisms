import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { login, logout } from 'redux-implicit-oauth2'

const config = {
    url: "https://api.sipgate.com/login/third-party/protocol/openid-connect/auth",
    client: "multi-sms-example",
    redirect: window.location,
    scope: "all"
}

const Login = ({ isLoggedIn, login, logout }) => {
    if (isLoggedIn) {
        return <button type='button' onClick={logout}>Logout</button>
    } else {
        return <button type='button' onClick={login}>Login</button>
    }
}

Login.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
    login: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired
}

const mapStateToProps = ({ auth }) => ({
    isLoggedIn: auth.isLoggedIn
})

const mapDispatchToProps = {
    login: () => login(config),
    logout
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
