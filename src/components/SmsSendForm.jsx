import React from "react";
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {addRecipientNumber, changeMessage, changeRecipientNumber, removeRecipientNumber, sendSms} from "../redux/sipgate/actions";

const Recipient = ({number, changeNumber}) => (
    <input type="text" value={number} onChange={changeNumber}/>
);

const Recipients = ({recipients, changeRecipientNumber, removeRecipientNumber}) => {
    const changeNumber = (recipientIdx) => (event) => {
        console.log("onchange");
        changeRecipientNumber(recipientIdx, event.target.value);
    };

    console.log(recipients);

    return <div>
        {Object.keys(recipients).map((idx) =>
            <div>
                <Recipient number={recipients[idx]} changeNumber={changeNumber(idx)}/>
                <button onClick={() => removeRecipientNumber(idx)}>-</button>
            </div>
        )}
    </div>


}

const SmsMessageInput = ({message, changeMessage}) => {
    const onChange = (event) => changeMessage(event.target.value);
    return <textarea onChange={onChange}>{message}</textarea>
}

const SmsSendForm = ({recipients, changeRecipientNumber, changeMessage, message, addRecipientNumber, removeRecipientNumber}) => {
    return <div>
        <SmsMessageInput message={message} changeMessage={changeMessage}/>
        <Recipients recipients={recipients} changeRecipientNumber={changeRecipientNumber} removeRecipientNumber={removeRecipientNumber}/>
        <button onClick={addRecipientNumber}>+</button>
    </div>
}

SmsSendForm.propTypes = {
    sendSms: PropTypes.func.isRequired,
    changeRecipientNumber: PropTypes.func.isRequired,
    addRecipientNumber: PropTypes.func.isRequired,
    changeMessage: PropTypes.func.isRequired,
};

const mapStateToProps = ({auth, sipgate}) => ({
    token: auth.token,
    recipients: sipgate.recipients,
    message: sipgate.message
})

const mapDispatchToProps = {
    sendSms, changeRecipientNumber, changeMessage, addRecipientNumber, removeRecipientNumber
};

export default connect(mapStateToProps, mapDispatchToProps)(SmsSendForm)
