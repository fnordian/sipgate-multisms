import React from "react";
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {sendSms} from "../redux/sipgate/actions";


const SmsSendButton = ({sendSms, token, recipients, message}) => {
    return <div>
        <button onClick={() => sendSms(token, message, recipients)}>send sms</button>
    </div>
}

SmsSendButton.propTypes = {
    sendSms: PropTypes.bool.isRequired,
};

const mapStateToProps = ({auth, sipgate}) => ({
    token: auth.token,
    recipients: sipgate.recipients,
    message: sipgate.message
})

const mapDispatchToProps = {
    sendSms
};

export default connect(mapStateToProps, mapDispatchToProps)(SmsSendButton)
