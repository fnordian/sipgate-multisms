import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'


const SmsSendStatus = ({ sending }) => {
    return <div>sending: {sending ? 'true' : 'false'}</div>
}

SmsSendStatus.propTypes = {
    sending: PropTypes.bool.isRequired,
};

const mapStateToProps = ({ sipgate }) => ({
    sending: sipgate.sending
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(SmsSendStatus)
